import { FormatModule, FrozenRowsModule, GroupRowsModule, PageModule, SortModule, Tabulator } from "tabulator-tables/dist/js/tabulator_esm";
Tabulator.registerModule([FormatModule, SortModule, PageModule, GroupRowsModule, FrozenRowsModule]);

export function sum(a, b) {
    return a + b;
}