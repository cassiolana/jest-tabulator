/** @type { import('@jest/types').Config.InitialOptions } */

const config = {
  testEnvironment: "jsdom",
  transform: {
    "\\.[jt]sx?$": "babel-jest"
  },
  transformIgnorePatterns: ["node_modules/(?!(tabulator-tables)/)"],
  verbose: false,
};

//export default config;
module.exports = config;