import { Tabulator } from "tabulator-tables";

jest.mock('tabulator-tables', () => {
    return {registerModule: () => {console.log("I'm a mock")}};
});


import { sum } from "./tab";

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});